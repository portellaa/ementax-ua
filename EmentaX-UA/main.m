//
//  main.m
//  EmentaX-UA
//
//  Created by Luis Portela Afonso on 1/10/13.
//  Copyright (c) 2013 Aveiro University. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
