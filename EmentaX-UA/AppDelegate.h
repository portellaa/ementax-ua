//
//  AppDelegate.h
//  EmentaX-UA
//
//  Created by Luis Portela Afonso on 1/10/13.
//  Copyright (c) 2013 Aveiro University. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
