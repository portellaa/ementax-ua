//
//  AppDelegate.m
//  EmentaX-UA
//
//  Created by Luis Portela Afonso on 1/10/13.
//  Copyright (c) 2013 Aveiro University. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
}

@end
